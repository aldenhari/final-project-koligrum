@add-new-quote
Feature: As a user I should be able to manipulate quotes

Scenario: User added new quote
    Given user is on quote startpage
    When user add new quotes : lol with yellow color, done with blue color, finish with cyan color, none with magenta color and all with white color
    Then user should see the quotes : lol with yellow color, done with blue color, finish with cyan color, none with magenta color and all with white color

Scenario: User deleting added quote by clicking it on grid view
    Given user has already added a yellow lol quote
    When user click the added yellow lol quote
    Then user will no longer see the yellow lol quote

Scenario: User expecting non-existent quote
    Given user is on quote startpage
    When user add new quotes : lol with yellow color, done with blue color, finish with cyan color, none with magenta color and all with white color
    Then user is expecting quote haha with black color that definitely doesn't exist
