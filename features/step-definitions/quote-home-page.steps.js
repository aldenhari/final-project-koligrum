import { Given, When, Then } from '@wdio/cucumber-framework';

import homePage from '../pageobjects/quote-home.page';


Given(/^user is on quote startpage$/, async () => {
    await homePage.open();
	await homePage.assertIsOnQuoteHomePage();
});

When(/^user add new quotes : lol with yellow color, done with blue color, finish with cyan color, none with magenta color and all with white color$/, async () => {
	await homePage.submitNewQuotes([
        {color:'yellow',text:'lol'},
        {color:'blue',text:'done'},
        {color:'cyan',text:'finish'},
        {color:'magenta',text:'none'},
        {color:'white',text:'all'},
    ]);
});

Then(/^user should see the quotes : lol with yellow color, done with blue color, finish with cyan color, none with magenta color and all with white color$/, async () => {
	await homePage.assertQuotesExistInGridView([
        {color:'yellow',text:'lol'},
        {color:'blue',text:'done'},
        {color:'cyan',text:'finish'},
        {color:'magenta',text:'none'},
        {color:'white',text:'all'},
    ]);
});

Given(/^user has already added a yellow lol quote$/, async () => {
	await homePage.open();
	await homePage.assertIsOnQuoteHomePage();
    await homePage.submitNewQuotes([
        {color:'yellow',text:'lol'},
    ]);
    await homePage.assertQuotesExistInGridView([
        {color:'yellow',text:'lol'},
    ]);
});

When(/^user click the added yellow lol quote$/, async () => {
	await homePage.deleteQuotesExistInGridView([
        {color:'yellow',text:'lol'},
    ]);
});

Then(/^user will no longer see the yellow lol quote$/, async () => {
	await homePage.assertQuotesIsNotExistInGridView([
        {color:'yellow',text:'lol'},
    ]);
});

Then(/^user is expecting quote haha with black color that definitely doesn't exist$/, async () => {
	await homePage.assertQuotesIsNotExistInGridView([
        {color:'black',text:'haha'},
    ]);
});
