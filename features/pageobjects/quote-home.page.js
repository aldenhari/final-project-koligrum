import Page from './page';

class quoteHomePage extends Page {
    get gridViewTab(){
        return $('#gridView');
    }
    get quoteFormField(){
        return $('#inputQuote');
    }
    get colorDropdownField(){
        return $('#colorSelect');
    }
    get addQuoteButton(){
        return $('#buttonAddQuote');
    }
    get allQuoteText(){
        return $('p[name="quoteText"]')
    }
    /**
     * overwrite specifc options to adapt it to page object
     */
    async open () {
        return browser.url(`http://bdd.atlasid.tech/`)
    }

    async assertIsOnQuoteHomePage(){
        await this.gridViewTab.waitForDisplayed(1000);
        await expect(await this.gridViewTab.parentElement()).toHaveAttributeContaining('class','active');
    }

    /**
     * 
     * @param {array} quotes : array of quotes object : [{color:"yellow",text:"something"},...]
     */
    async submitNewQuotes(quotes){
        const colorDropdownItems = ["white","yellow","cyan","magenta","blue"];
        await this.quoteFormField.waitForDisplayed(1000);
        await this.colorDropdownField.waitForDisplayed(1000);
        await this.addQuoteButton.waitForDisplayed(1000);
        quotes.map(async(quote)=>{
            const dropdownIndex = colorDropdownItems.findIndex(color=>color===quote.color);
            if(dropdownIndex===-1) throw new Error();
            /** INPUT QUOTE TEXT FIELD */
            await this.quoteFormField.setValue(quote.text);
            /** SELECTING COLOR DROPDOWN */
            await this.colorDropdownField.selectByIndex(dropdownIndex);
            /** SUBMIT CLICK */
            await this.addQuoteButton.click();
        });
    }

    /**
     * 
     * @param {array} expectedQuotes : array of quotes object : [{color:"yellow",text:"something"},...]
     */
    async assertQuotesExistInGridView(expectedQuotes){
        await this.allQuoteText.waitForDisplayed(1000);
        
        const quoteTextElements = await this.allQuoteText;
        expectedQuotes.map(async(quote)=>{
            /** MECHANISM
             *  - get quote text element with matching value with quote.text
             *  - get quotePanel parent element of the quoteText element that contains the color information
             *  - check whether the color and text are matching or not
             */
            const quoteTextElementIndex = quoteTextElements.findIndex(async (element)=>{
                const quoteTextValue = await element.getText();
                const quotePanelElement = await element.parentElement().parentElement();
                const quotePanelBackgroundColor = await quotePanelElement.getCSSProperty('background-color');
                if(quoteTextValue===quote.text && quotePanelBackgroundColor===quote.color) return true;
                return false;
            });
            if(quoteTextElementIndex===-1) throw new Error(`quoteText element not found for color : ${quote.color} and text : ${quote.text}`);
        });
    }

    /**
     * 
     * @param {array} expectedQuotes : array of quotes object : [{color:"yellow",text:"something"},...]
     */
     async assertQuotesIsNotExistInGridView(expectedQuotes){
        await this.allQuoteText.waitForDisplayed(1000);
        const quoteTextElements = await this.allQuoteText;
        expectedQuotes.map(async(quote)=>{
            /** MECHANISM
             *  - get quote text element with matching value with quote.text
             *  - get quotePanel parent element of the quoteText element that contains the color information
             *  - check whether the color and text are matching or not
             */
            const quoteTextElementIndex = quoteTextElements.findIndex(async (element)=>{
                const quoteTextValue = await element.getText();
                const quotePanelElement = await element.parentElement().parentElement();
                const quotePanelBackgroundColor = await quotePanelElement.getCSSProperty();
                if(quoteTextValue===quote.text && quotePanelBackgroundColor===quote.color) return true;
                return false;
            });
            if(quoteTextElementIndex!==-1) throw new Error(`quoteText element found for color : ${quote.color} and text : ${quote.text}`);
        });
    }

    /**
     * 
     * @param {array} targetQuotes : array of quotes object : [{color:"yellow",text:"something"},...]
     */
     async deleteQuotesExistInGridView(targetQuotes){
        await this.allQuoteText.waitForDisplayed(1000);
        const quoteTextElements = await this.allQuoteText;
        targetQuotes.map(async(quote)=>{
            /** MECHANISM
             *  - get quote text element with matching value with quote.text
             *  - get quotePanel parent element of the quoteText element that contains the color information
             *  - check whether the color and text are matching or not
             */
            const quoteTextElementIndex = quoteTextElements.findIndex(async (element)=>{
                const quoteTextValue = await element.getText();
                const quotePanelElement = await element.parentElement().parentElement();
                const quotePanelBackgroundColor = await quotePanelElement.getCSSProperty();
                if(quoteTextValue===quote.text && quotePanelBackgroundColor===quote.color) return true;
                return false;
            });
            if(quoteTextElementIndex===-1) throw new Error(`quoteText element not found for color : ${quote.color} and text : ${quote.text}`);
            await quoteTextElements[quoteTextElementIndex].click();
        });
    }
    
}
export default new quoteHomePage();